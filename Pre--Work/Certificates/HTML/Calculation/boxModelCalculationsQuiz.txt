Box Model Quiz:

- Total height: 20px + 1px + 10px + 150px + 10px + 1px + 20px = 212px

- Total width: 20px + 1px + 10px + 400px + 10px + 1px + 20px = 462px

- Browser calculated height: 1px + 10px + 400px + 10px + 1px = 422px

- Browser calculated width: 1px + 10px + 150px + 10px + 1px = 172px

*Please refer to the following links in my repository to conclude how I came up with the equations above:

HTML File - https://bitbucket.org/hseligson1023/swcguild/src/d787ab3bbc22854dfa13a5e7071e623d32b0a870/Pre--Work/Certificates/HTML/Calculation/index.html?fileviewer=file-view-default

CSS File - https://bitbucket.org/hseligson1023/swcguild/src/d787ab3bbc22854dfa13a5e7071e623d32b0a870/Pre--Work/Certificates/HTML/Calculation/style/boxModelCalculationsQuiz.css?fileviewer=file-view-default

Thank you!