console.log("Javascript is attached!");

function validateForm() {
	var name = document.getElementById("name").value,
		email = document.getElementById("email").value,
		dd = document.getElementById("reasons"),
		selectedValue = dd.options[dd.selectedIndex].value,
		info = document.getElementById("additional-info").value,
		cb = document.getElementsByName("mychoices"),
		isFalse = false;

	if (name == null || name == "" || email == null || email == ""){
		alert("Name and email must be provided");
	}

	if (selectedValue == "Other" && info == ""){
		alert("You selected Other. Please provide additional information.");
	}
	
	for(var i=0,l=cb.length; i<l; i++){
		if( cb[i].checked ) {
			isFalse=true;
			break;
		}
	}
	if( !isFalse ) {
		alert("You must select a day that is best to contact you.");
	}
}