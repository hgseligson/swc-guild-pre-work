//Hide the table when the page loads
document.getElementById("results").style.visibility = "hidden";

//Set up the game
function luckySevens() {
	// capture the amount the player is betting
	// create two die
	// roll both die
	// if the sum of those two die are 7 add 4 dollars
	// if the sum of those two die are not equal to 7 take away 1 dollar 
	// when the game is complete show the results table
	// reflect the starting bet in the table
	// reflect the number of rolls before going broke in the table
	// reflect the highest amount won
	// reflect the roll count of the two die at the highest amount won
	// remember to give the user an opportunity to play again

	var bet = document.getElementById("bet").value;
	var monies = Number(bet);
	var winnings = 0;
	var rolls = 0;
	
	while( monies > 0){
		var d1 = Math.floor((Math.random() * 6) + 1);
		var d2 = Math.floor((Math.random() * 6) + 1);
		var dTotal = d1 + d2;

		rolls++;


		if( dTotal == 7 ){
			monies += 4;
			winnings += 4;
			if( monies > bet ){
				var mostMoneyRolls = rolls;
			}

		}else {
			monies -= 1;
		}
	}
	if( monies <= 0 ){
		document.getElementById("results").style.visibility = "visible";
		document.getElementById("playbutton").value = "Play Again?";
	}

	document.getElementById("starting-bet").innerHTML = bet;
	document.getElementById("total-rolls").innerHTML = rolls;
	document.getElementById("highest-amount-won").innerHTML = winnings;
	document.getElementById("roll-count-at-highest-amount").innerHTML = mostMoneyRolls;

}
